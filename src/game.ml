module rec Strategy: sig
	type t = {
		pick: game:State.t -> choices:Scoring.t list -> Scoring.t;
		bank: game:State.t -> bool;
		keep_run: game:State.t -> bool;
	}
end = struct
	type t = {
		pick: game:State.t -> choices:Scoring.t list -> Scoring.t;
		bank: game:State.t -> bool;
		keep_run: game:State.t -> bool;
	}
end

and Player: sig
	type t = {
		id: string (* for display only *);
		banked: Points.t;
		strategy: Strategy.t;
	}

	val make: string -> Strategy.t -> t

	val bank: t -> int -> t

	val name: t -> string

	val banked: t -> Points.t

	val pp: Format.formatter -> t -> unit
end = struct
	type t = {
		id: string;
		banked: Points.t;
		strategy: Strategy.t;
	}

	let make id strategy = {id; banked = Points.zero; strategy}

	let bank player score = {player with banked = Points.add player.banked score}

	let name {id; _} = id

	let banked {banked; _} = banked

	let pp fmt {id; banked; _} = Format.fprintf fmt "%s (%d)" id (banked :> int)
end

and State: sig
	type t = {
		players: Player.t list (* invariant: non-empty *);
		running: int;
		dice: int;
	}
end = struct
	type t = {
		players: Player.t list;
		running: int;
		dice: int;
	}
end

(* GAME PARAMETER: The game is played with five (5) dice. *)
let number_of_dice = 5

let make players = {State.players; running = 0; dice = number_of_dice}

let pp fmt {State.players; running; dice} =
	let open Format in
	fprintf fmt
		"Dice: %d\n\
		 Running points: %d\n\
		 You:%a\n\
		 Others:%a\n"
		dice
		running
		Player.pp (List.hd players)
		(pp_print_list
			~pp_sep:(fun fmt () -> pp_print_string fmt " - ")
			Player.pp)
		(List.tl players)

let current_player game = List.hd game.State.players

let next_player (game: State.t) =
	{game with players = Utils.List.rotate game.players}

let reset_turn (game: State.t) = {game with running = 0; dice = number_of_dice}

let bank (game: State.t) =
	{
		game with
		players =
			Utils.List.map_head (fun p -> Player.bank p game.running) game.players;
	}

let rec play_turn (game: State.t) =
	(* GAME RULE: The current player rolls all the available dice. *)
	let roll = D6s.roll game.dice in
	match Scoring.choices roll with
	| [] ->
			(* GAME RULE: When there are no parts of the roll that can be used to
			   score points, the current player's turn ends, and the next player
			   starts with a fresh counter and dice set. *)
			reset_turn game
	| _ :: _ as choices ->
			let player = current_player game in
			let choice = player.strategy.pick ~game ~choices in
			assert (List.mem choice choices) (* Check for cheating *);
			let choice_score = Scoring.score choice in
			let choice_length = Scoring.cardinal choice in
			let new_dice = game.dice - choice_length in
			(* GAME RULE: When a player picks all the available dice to contribute
			   to the score, a fresh new set of dice becomes available. *)
			let new_dice = if new_dice = 0 then number_of_dice else new_dice in
			let game =
				{game with running = game.running + choice_score; dice = new_dice}
			in
			(* The player can then decide to bank the score or not *)
			if player.strategy.bank ~game then
				bank game (* bank and finish turn *)
			else
				play_turn game (* keep playing the turn *)

(* `play` is the core engine of the game, it calls `play_turn` in order to
   make the game progress through a single turn, and handles the handing over
   from player to next player in between turns. *)
let rec play (game: State.t) =
	(* 01: If the previous player's run is unfinished, the current player
	   chooses whether to reset the dice or continue on the previous player's
	   run. *)
	let game =
		if game.running <> 0 && (current_player game).strategy.keep_run ~game then
			game
		else
			reset_turn game
	in
	(* 02: the current player plays a turn *)
	let game = play_turn game in
	if (current_player game).banked = Points.target then
		(* 03: if the current player has reach 10000 points then the game ends *)
		game (* Game is finished, return the current state *)
	else
		(* 04: otherwise start with the next player. *)
		next_player game |> play
