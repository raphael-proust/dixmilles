let me (game: Game.State.t) = List.hd game.players

let mine (game: Game.State.t) = Game.Player.banked (me game)

let with_recognise_win_picker picker ~game ~choices =
  let winning =
    List.fold_left
      (fun winning choice ->
        match winning with
        | Some _ -> winning
        | None ->
            let can_get = Points.add (mine game) (Scoring.score choice) in
            if can_get = Points.target then
              Some choice
            else
              None)
      None choices
  in
  match winning with
  | Some winning -> winning
  | None -> picker ~game ~choices

let with_recognise_win_banker banker ~game =
  Points.(add (mine game) game.running = target) || banker ~game

let with_recognise_overflow_banker banker ~game =
  let current_points = mine game in
  if Points.(add current_points game.running <= current_points) then
    false
  (* keep playing until you lose the turn (and get 0 points) rather
     than bank points that bring you down. *)
  else
    banker ~game

let with_recognise_overflow_keeper keeper ~game =
  let current_points = mine game in
  if Points.(add current_points game.running <= current_points) then
    false
  else
    keeper ~game

let with_basics {Game.Strategy.pick; bank; keep_run} =
  {
    Game.Strategy.pick = with_recognise_win_picker pick;
    bank = with_recognise_overflow_banker @@ with_recognise_win_banker bank;
    keep_run = with_recognise_overflow_keeper keep_run;
  }

(* `most_points` is a picker function that takes the highest scoring choice
   available. *)
let most_points ~game:_ ~choices =
  match choices with
  | [] -> assert false
  | choice :: choices ->
      fst @@
      List.fold_left
        (fun (chosen, score) choice ->
          let competing_score = Scoring.score choice in
          if competing_score > score then
            (choice, competing_score)
          else
            (chosen, score))
        (choice, Scoring.score choice)
        choices

(* `always_bank_never_keep` and `bank_when_one_dice` are mostly just
   assemblages of the helper functions above. *)
let always_bank_never_keep =
  with_basics
    {
      Game.Strategy.pick = most_points;
      bank = (fun ~game:_ -> true);
      keep_run = (fun ~game:_ -> false);
    }

let bank_when_one_dice =
  with_basics
    {
      Game.Strategy.pick = most_points;
      bank = (fun ~game -> game.Game.State.dice = 1);
      keep_run = (fun ~game -> game.Game.State.dice > 1);
    }
