module List = struct
  let repeat n x =
    let rec repeat acc n =
      if n <= 0 then
        acc
      else
        repeat (x :: acc) (n - 1)
    in
    repeat [] n

  let bind f x = List.flatten (List.map f x)

  let rotate l = List.tl l @ [List.hd l]

  let map_head f l = f (List.hd l) :: List.tl l
end
