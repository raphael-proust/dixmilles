type t = int array

let index_of_face =
	let open D6 in
	function
	| One -> 0
	| Two -> 1
	| Three -> 2
	| Four -> 3
	| Five -> 4
	| Six -> 5

let init_empty () = [|0; 0; 0; 0; 0; 0|]

let empty = init_empty ()

let roll n =
	if n < 0 then
		raise (Invalid_argument "D6s.roll")
	else
		let acc = init_empty () in
		for _ = 1 to n do
		  let i = Random.int 6 in
		  acc.(i) <- acc.(i) + 1
		done;
		acc

let make ds =
	let acc = init_empty () in
	List.iter
		(fun d ->
		  let i = index_of_face d in
		  acc.(i) <- acc.(i) + 1)
		ds;
	acc

let ones t = t.(0)
let twos t = t.(1)
let threes t = t.(2)
let fours t = t.(3)
let fives t = t.(4)
let sixes t = t.(5)

let unsafe_diff = Array.map2 ( - )

let diff a b =
	let r = unsafe_diff a b in
	if Array.exists (fun x -> x < 0) r then
		raise (Invalid_argument "D6s.diff")
	else
		r

let add = Array.map2 ( + )

let subset ~small ~big = Array.for_all (fun x -> 0 <= x) (unsafe_diff big small)

module Algebra = struct
	let ( <= ) small big = subset ~small ~big
	let ( - ) = diff
	let ( + ) = add
end

let cardinal = Array.fold_left ( + ) 0

let pp_n_times fmt n f =
	for _ = 1 to n do
		Format.pp_print_string fmt f
	done

let pp fmt t =
	pp_n_times fmt t.(0) "⚀";
	pp_n_times fmt t.(1) "⚁";
	pp_n_times fmt t.(2) "⚂";
	pp_n_times fmt t.(3) "⚃";
	pp_n_times fmt t.(4) "⚄";
	pp_n_times fmt t.(5) "⚅"
