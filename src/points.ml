type t = int

(* GAME RULE: The aim is to get ten thousand points. *)
let target = 10_000

let zero = 0

let add t n =
	let sum = t + n in
	if sum <= target then
		sum
	else
		(* GAME RULE: if you go beyond the target score, your score "bounces".
		   E.g., if you are 250 points from the target and you score 400 points
		   then 250 points are used to increase your score up to the target score,
		   the remainder (400 - 250 = 150 points) decreases your score down from
		   the target score. *)
		target - (sum - target)
