type atom = D6s.t

type t = atom list

let scorer: (atom * int) list =
	let open D6 in (* for `One`, `Two`, etc. *)
	let open D6s in (* for `make: D6.t list -> D6s.t` *)
	[
		(* GAME RULE: One is worth 100 points on its own *)
		(make [One], 100);
		(* GAME RULE: Repetitions of Ones are worth points as follows: 1000 for
		   three Ones, 10 times more for each additional One. *)
		(make [One; One; One], 1_000);
		(make [One; One; One; One], 10_000);
		(make [One; One; One; One; One], 100_000);
		(* GAME RULE: Repetitions of all other numbers are worth points as
		   follows: Dx100 for three Ds, 10 times more for each additional D.  *)
		(make [Two; Two; Two], 200);
		(make [Two; Two; Two; Two], 2_000);
		(make [Two; Two; Two; Two; Two], 20_000);
		(make [Three; Three; Three], 300);
		(make [Three; Three; Three; Three], 3_000);
		(make [Three; Three; Three; Three; Three], 30_000);
		(make [Four; Four; Four], 400);
		(make [Four; Four; Four; Four], 4_000);
		(make [Four; Four; Four; Four; Four], 40_000);
		(* GAME RULE: Five is worth 50 points on its own. *)
		(make [Five], 50);
		(make [Five; Five; Five], 500);
		(make [Five; Five; Five; Five], 5_000);
		(make [Five; Five; Five; Five; Five], 50_000);
		(make [Six; Six; Six], 600);
		(make [Six; Six; Six; Six], 6_000);
		(make [Six; Six; Six; Six; Six], 60_000);
	]

let v = List.map fst scorer

(* Score an atom by looking it up in the list `v` *)
let score_atom c = List.assoc c scorer

(* Score a `t` by summing the scores of all its atoms *)
let score c = List.fold_left (fun acc c -> acc + score_atom c) 0 c

let cardinal c = List.fold_left (fun acc c -> acc + D6s.cardinal c) 0 c

let rec after xs y =
	match xs with
	| [] -> []
	| z :: zs ->
		if y = z then
			xs
		else
			after zs y

let rec choices t v =
	let open D6s.Algebra in
	v
	|> List.filter (fun pick -> pick <= t)
	|> Utils.List.bind (fun pick ->
			[pick] ::
			List.map
				(fun further_choice -> pick :: further_choice)
				(choices (t - pick) (after v pick)))

(* Shadowing `choices` so it is always applied to the expected `v`. *)
let choices t = choices t v

(* Boilerplate code for pretty-printing *)
let pp_atom fmt a = Format.fprintf fmt "%a(%d)" D6s.pp a (score_atom a)

let pp fmt d =
	let open Format in
	fprintf fmt
		"%a (%d)"
		(pp_print_list
			~pp_sep:(fun fmt () -> pp_print_string fmt "·")
			pp_atom)
		d
		(score d)

