type atom = private D6s.t

val v: atom list

type t = atom list

val score: t -> int

val cardinal: t -> int

val choices: D6s.t -> t list

val pp: Format.formatter -> t -> unit
