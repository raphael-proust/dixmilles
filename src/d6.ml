type t = One | Two | Three | Four | Five | Six

let roll () =
	match Random.int 6 with
	| 0 -> One
	| 1 -> Two
	| 2 -> Three
	| 3 -> Four
	| 4 -> Five
	| 5 -> Six
	| _ -> assert false

let pp fmt = function
	| One -> Format.pp_print_string fmt "⚀"
	| Two -> Format.pp_print_string fmt "⚁"
	| Three -> Format.pp_print_string fmt "⚂"
	| Four -> Format.pp_print_string fmt "⚃"
	| Five -> Format.pp_print_string fmt "⚄"
	| Six -> Format.pp_print_string fmt "⚅"
