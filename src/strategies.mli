(* `always_bank_never_keep` is a simple strategy that always banks points
   whenever possible and never keeps the previous run. This is a low-risk
   strategy that favours scoring points often but little by little rather than
   building up big scores occasionally.
*)
val always_bank_never_keep: Game.Strategy.t

(* `bank_when_one_dice` is a strategy that banks the points when there is only
    one dice left. This is a strategy that has a potential to build up bigger
    score, but is also taking more risks and thus more often ends up losing a
    turn. *)
val bank_when_one_dice: Game.Strategy.t
