let seed =
	let open Cmdliner.Arg in
	let docv = "SEED" in
	let doc = "The seed for the PRNG" in
	value & opt (some int) None & info ["s"; "seed"] ~doc ~docv

let players =
	let open Cmdliner.Arg in
	let doc =
		"The players to pit against each other. Available $(docv) are \
		 $(b,risk-averse) and $(b,1-dice)."
	in
	let strategies = [
		("risk-averse", Strategies.always_bank_never_keep);
		("1-dice", Strategies.bank_when_one_dice);
	] in
	non_empty & pos_all (enum strategies) [] & info [] ~doc ~docv:"STRATEGY"

let main seed players =

	let () =
		match seed with
		| None -> Random.self_init ()
		| Some seed -> Random.init seed
	in

	let players =
		let player_num = ref 0 in
		List.map
			(fun strategy ->
				incr player_num;
				Game.Player.make (Format.asprintf "player_%d" !player_num) strategy)
			players
	in

	let g = Game.make players in

	let g = Game.play g in

	let () =
		let open Format in
		printf "%a"
			(pp_print_list
				~pp_sep:pp_print_newline
				(fun fmt player ->
					fprintf fmt "%s: %d"
						(Game.Player.name player)
						(Game.Player.banked player :> int)))
			g.Game.State.players
	in

	()

let main_t =
	let open Cmdliner.Term in
	const main $ seed $ players

let info = Cmdliner.Term.info "dixmilles" ~doc:"Play a dice game"

let () =
	match Cmdliner.Term.eval (main_t, info) with
	| `Error _ -> exit 1
	| _ -> exit 0
