type t = private int

val zero: t

val add: t -> int -> t

val target: t
