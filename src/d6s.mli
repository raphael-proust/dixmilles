(* The type of a dice roll: a multiset of 6-sided dice that have be rolled. *)
type t

(* [empty] is a dice roll for zero (0) dice. *)
val empty: t

(* [make d6s] is a dice roll for the dice in [d6s]. *)
val make: D6.t list -> t

(* [roll n] is a dice roll of [n] dice.
   @raise [Invalid_argument] if [n < 0]
*)
val roll: int -> t

(* [ones t] is the number of dice that have rolled one (1) in [t].
   E.g., the following expressions are [true]:
   [ones (make D6.[]) = 0],
   [ones (make D6.[One;]) = 1],
   [ones (make D6.[One; One]) = 2],
   [ones (make D6.[Two]) = 0], and
   [ones (make D6.[One; Two; Three]) = 1].
*)
val ones: t -> int

(* [twos], [threes], [fours], [fives], [sixes]. See [ones] above. *)
val twos: t -> int
val threes: t -> int
val fours: t -> int
val fives: t -> int
val sixes: t -> int

(* [add t1 t2] is a dice roll that includes the rolls from both [t1] and [t2].
   In other words, forall [d6: D6.t],
   [faces d6 (add t1 t2) = faces d1 t1 + faces d6 t2].
*)
val add: t -> t -> t

(* [diff t1 t2] is a dice roll that includes the rolls from [t1] with the rolls
   from [t2] subtracted.
   In other words, forall [d6: D6.t],
   [faces d6 (diff t1 t2) = faces d1 t1 - faces d6 t2].
   @raise [Invalid_argument] if not [subset ~small:t2 ~big:t1].
*)
val diff: t -> t -> t

(* [subset ~small ~big] is [true] iff [big] is a dice roll that includes the
   dice from [small]. In other words [subset ~small ~big] is [true] iff forall
   [d6: D6.t], [face d6 small <= face d6 big]. *)
val subset: small:t -> big:t -> bool

(* Infix operators for [diff], [add], and [subset]. *)
module Algebra: sig
	val ( - ): t -> t -> t
	val ( + ): t -> t -> t
	val ( <= ): t -> t -> bool
end

(* [cardinal t] is the number of dice rolled in [t].
   E.g., the following statements are [true]:
   [cardinal (roll n) = n],
   [cardinal empty = 0], and
   [cardinal (add t1 t2) = cardinal t1 + cardinal t2].
*)
val cardinal: t -> int

(* pretty printing *)
val pp: Format.formatter -> t -> unit
