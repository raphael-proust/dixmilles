module rec Strategy: sig
	type t = {
		pick: game:State.t -> choices:Scoring.t list -> Scoring.t;
		bank: game:State.t -> bool;
		keep_run: game:State.t -> bool;
	}
end

and Player: sig
	type t

	val make: string -> Strategy.t -> t

	val name: t -> string

	val banked: t -> Points.t

	val pp: Format.formatter -> t -> unit
end

and State: sig
	type t = {
		players: Player.t list (* invariant: non-empty *);
		running: int;
		dice: int;
	}
end

val make: Player.t list -> State.t

val pp: Format.formatter -> State.t -> unit

val play: State.t -> State.t
