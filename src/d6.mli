type t = One | Two | Three | Four | Five | Six

val roll: unit -> t

val pp: Format.formatter -> t -> unit
